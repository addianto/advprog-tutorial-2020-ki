package id.ac.ui.cs.tutorial4.dataclass;

public class Paper {
    private String title;
    private String abstrak;
    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public String getAbstrak() {
        return abstrak;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Paper{" + "tittle='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
